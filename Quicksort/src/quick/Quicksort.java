package quick;

import java.util.Random;

public class Quicksort {

	public static void quicksort(int[] zahlenliste, int links, int rechts) {
		int index = partition(zahlenliste, links, rechts);
		
		if (links < index-1) {
			quicksort(zahlenliste, links, index-1);
		}
		if(index < rechts) {
			quicksort(zahlenliste, index, rechts);
		}
		
	}
	
	public static int  partition(int zahlenliste[], int links, int rechts) {
		
		int pivot = zahlenliste[(links + rechts)/2];
		
		while(links <= rechts) {
			
			while(zahlenliste[links] < pivot) links++;
			while(zahlenliste[rechts] > pivot) rechts--;

		if (links <= rechts) {
			
			int tmp = zahlenliste[links];
			zahlenliste[links] = zahlenliste[rechts];
			zahlenliste[rechts] = tmp;
			
			links++;
			rechts--;
			
		}
		
		}
		
		return links;
	}
	
	public static void main(String[] args) {
		
		Random rand = new Random();
		
		int[] zahlenListe = new int[40];
		
		for (int i = 0; i < zahlenListe.length; i++) {
			zahlenListe[i] = rand.nextInt(100)+1;
		}
		
		quicksort(zahlenListe, 0, zahlenListe.length-1);
		
		for (int i = 0; i < zahlenListe.length; i++) {
			System.out.print(zahlenListe[i] +", ");
		}
	}

}
