package de.futurehome.tanksimulator;
public class Tank {
	
	private double fuellstand;
	private double prozentZahl;

	public Tank(double fuellstand, double prozent) {
		this.fuellstand = fuellstand;
		this.prozentZahl = prozent;
	}

	public double getFuellstand() {
		return fuellstand;
	}

	public void setFuellstand(double fuellstand) {
		this.fuellstand = fuellstand;
	}
	
	public void setProzent(double prozent) {
		this.prozentZahl=prozent;
	}
	
	public double getProzent() {
		return prozentZahl;
	}

}
