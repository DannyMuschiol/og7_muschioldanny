package keystore;

import java.util.ArrayList;

public class KeyStore02 {

	private ArrayList<String> keyListe;
	
	
	public KeyStore02() {
		keyListe = new ArrayList<String>();
	}

	
	public boolean add(String e) {
		
		return keyListe.add(e);
		
	}
	
	public void clear() {
		
		keyListe.clear();
		
	}	
	
	
	public int indexOf(String str) {
		
		return keyListe.indexOf(str);

	}
	
	public int size() {
		return keyListe.size();
	}
	
	public void remove(int index) {
		
		keyListe.remove(index);
		
	}
	
	public boolean remove(String str) {
		
		return keyListe.remove(str);
		
	}
	
	
	public String toString() {
		
		return keyListe.toString();
		
	}
	
	public String get(int index) {
		
		return keyListe.get(index);
		
	}
	
}
