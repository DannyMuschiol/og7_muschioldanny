package keystore;

public class KeyStore01 {

	private String[] keyListe;
	
	
	public KeyStore01() {
		keyListe = new String[100];
	}
	
	public KeyStore01(int length) {
		this.keyListe = new String[length];
	}
	
	public boolean add(String e) {
		
		for (int i = 0; i < keyListe.length; i++) {
			if (keyListe[i]  == null) {
			keyListe[i] = e;
			return true;
			}
			
		}
		
			return false;
	}
	
	public void clear() {
		
		for (int i = 0; i < keyListe.length; i++) {
			keyListe[i] = null;
		}
		
	}	
	
	
	public int indexOf(String str) {
		
		for (int i = 0; i < keyListe.length; i++) {
			if (keyListe[i] != null && keyListe[i].equals(str)) {
				return i;
			}
			 
		}
		return -1;
	}
	
	public int size() {
		int anzahl = 0;
		for (int i = 0; i < keyListe.length; i++) {
			if(keyListe[i] != null) {
				anzahl++;
			}
		}
		
		return anzahl;
	}
	
	public void remove(int index) {
		
		keyListe[index] = null;
		
		for (; index < keyListe.length-1; index++) {
			keyListe[index] = keyListe[index+1];
		}
		keyListe[keyListe.length-1] = null;
	}
	
	public boolean remove(String str) {
		
		for (int i = 0; i < keyListe.length; i++) {
			if(keyListe[i].equals(str)) {
				keyListe[i] = null;
			}
			else {
				return false;
			}
		}
		
		return true;
	}
	
	
	public String toString() {
		String liste = "";
		
		for (int i = 0; i < keyListe.length; i++) {
			
			if(keyListe[i] != null) {
			
				liste = liste + keyListe[i] +"\n";
			
			}
			
		}
		
		return liste;
	}
	
	public String get(int index) {
		
		return keyListe[index];
	}
	
}
