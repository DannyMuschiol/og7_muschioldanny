package stringsort;

import java.util.ArrayList;

public class Stringsortierer {

	public static void main(String[] args) {

		ArrayList<String> liste = new ArrayList<String>();

		liste.add("Asshabi Luc");
		liste.add("Meta Tim");
		liste.add("Faggot Arslan");
		
		System.out.println(StringSuche(liste, "meta tim"));
		StringSort(liste);
		
		System.out.println(liste);
		
		/*for (int i = 0; i < liste.size(); i++) {
			System.out.println(liste.get(i));
		}*/

	}

	public static int StringSuche(ArrayList<String> liste, String gesucht) {
		
		for (int i = 0; i < liste.size() ; i++) {
			if (liste.get(i).equalsIgnoreCase(gesucht)) {
				return i;
			}
		}
		
		return -1;
	
	}

	public static void StringSort(ArrayList<String> liste) {
		
		for (int i = 1; i < liste.size(); i++) {
			for (int j = 0; j < liste.size()-i; j++) {
				
				if (liste.get(j).compareTo(liste.get(j+1)) > 0) {
					String tmp = liste.get(j);
					liste.set(j, liste.get(j+1));
					liste.set(j+1, tmp);
				}
				
			}
		}
		
	}
	
}
