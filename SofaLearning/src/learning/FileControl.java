package learning;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileControl {

	public static void main(String[] args) {
		
		File f = new File("E:\\miriam.dat");
		
		dateiEinlesenUndAusgeben(f);
		
	}
	
	public static void dateiEinlesenUndAusgeben(File file) {	
		
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			String s = br.readLine();
			
			while(s != null) {
				System.out.println(s);
				s = br.readLine();
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
