package pers;

public class Mannschaftsleiter extends Spieler {

	private String mannschaftsname;
	private boolean rabatt;
	
	public Mannschaftsleiter(String mannschaftsname, boolean rabatt) {
		super(0, "");
	}
	
	public String getMannschaftsname() {
		return mannschaftsname;
	}
	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}
	public boolean isRabatt() {
		return rabatt;
	}
	public void setRabatt(boolean rabatt) {
		this.rabatt = rabatt;
	}
	
	
	
}
