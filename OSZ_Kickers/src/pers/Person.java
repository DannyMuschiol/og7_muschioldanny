package pers;

public class Person {

	protected String name;
	protected String telefonnummer;
	protected boolean mitgliedsbeitrag;
	
	public Person(String name, String telefonnummer, boolean mitgliedsbeitrag) {
		this.name=name;
		this.telefonnummer=telefonnummer;
		this.mitgliedsbeitrag=mitgliedsbeitrag;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public boolean isMitgliedsbeitrag() {
		return mitgliedsbeitrag;
	}
	public void setMitgliedsbeitrag(boolean mitgliedsbeitrag) {
		this.mitgliedsbeitrag = mitgliedsbeitrag;
	}
	
	
	
}
