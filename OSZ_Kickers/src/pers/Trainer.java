package pers;

import javax.sql.rowset.CachedRowSet;

public class Trainer extends Person {

	private String lizenzklasse;
	private int aufwandentschaedigung;

	public Trainer(String lizenzklasse) {
		super("", "", true);
		this.lizenzklasse = lizenzklasse;
	}

	public String getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(String lizenzklasse) {
		String lizenz = "";
		switch (lizenz) {
		case "A":
			lizenzklasse = "A";
			break;
		case "B":
			lizenzklasse = "B";
			break;
		case "C":
			lizenzklasse = "C";
			break;
		default:
			lizenzklasse = "Fehler";
		}
	}

	public int getAufwandentschaedigung() {
		return aufwandentschaedigung;
	}

	public void setAufwandentschaedigung() {

		if (lizenzklasse == "A") {
			aufwandentschaedigung = 125;
		}

		if (lizenzklasse == "B") {
			aufwandentschaedigung = 250;
		}

		if (lizenzklasse == "C") {
			aufwandentschaedigung = 450;
		}

	}

}
