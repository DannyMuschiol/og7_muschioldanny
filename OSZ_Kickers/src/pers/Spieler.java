package pers;

public class Spieler extends Person{

	protected int trikotNr;
	protected String spielposition;
	
	public Spieler(int trikotNr, String spielposition) {
		super("", "", true);
		this.trikotNr=trikotNr;
		this.spielposition=spielposition;
	}
	
	public int getTrikotNr() {
		return trikotNr;
	}
	public void setTrikotNr(int trikotNr) {
		this.trikotNr = trikotNr;
	}
	public String getSpielposition() {
		return spielposition;
	}
	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}
	
	
	
}
