package pers;

public class TestKickers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Trainer tim=new Trainer("C");
		tim.setName("Tim Wolf");
		tim.setTelefonnummer("01513456789");
		tim.setMitgliedsbeitrag(true);
		tim.setAufwandentschaedigung();
		System.out.println("Schiedsrichter:" +"\nName: " +tim.getName() +"\nTelefonnummer: " +tim.getTelefonnummer() +"\nBeitrag: " +tim.isMitgliedsbeitrag());
		System.out.println("Lizenzklasse: " +tim.getLizenzklasse() +"\nAufwandentsch�digung: " +tim.getAufwandentschaedigung());
		System.out.println("-------------------------------------------------------------------------------------------------");
		
		Schiedsrichter luc=new Schiedsrichter(20);
		luc.setName("Luc Asshabi");
		luc.setTelefonnummer("01761234567");
		luc.setMitgliedsbeitrag(true);
		System.out.println("Schiedsrichter:" +"\nName: " +luc.getName() +"\nTelefonnummer: " +luc.getTelefonnummer() +"\nBeitrag: " +luc.isMitgliedsbeitrag() +"\nSpiele: " +luc.getSpiele());
		System.out.println("-------------------------------------------------------------------------------------------------");
		
		Spieler tom=new Spieler(1, "Tor");
		tom.setName("Tom Schrogl");
		tom.setTelefonnummer("01767654321");
		tom.setMitgliedsbeitrag(false);
		System.out.println("Spieler:" +"\nName: " +tom.getName() +"\nTelefonnummer: " +tom.getTelefonnummer() +"\nBeitrag: " +tom.isMitgliedsbeitrag());
		System.out.println("Trikot Nr: " +tom.getTrikotNr() +"\nPosition: " +tom.getSpielposition());
		System.out.println("-------------------------------------------------------------------------------------------------");
		
		Mannschaftsleiter danny=new Mannschaftsleiter("OSZ-Kickers", false);
		danny.setName("Danny Muschiol");
		danny.setTelefonnummer("01764567891");
		danny.setMitgliedsbeitrag(true);
		danny.setTrikotNr(1);
		danny.setSpielposition("Tor");
		System.out.println("Mannschaftsleiter:" +"\n" +danny.getName() +"\nTelefonnummer: " +danny.getTelefonnummer() +"\nBeitrag: " +danny.isMitgliedsbeitrag());
		System.out.println("Trikot Nr:" +danny.getTrikotNr() +"\nPosition: " +danny.getSpielposition() +"\nRabatt: " +danny.isRabatt());
	}

}
