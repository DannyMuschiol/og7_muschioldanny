package pers;

public class Schiedsrichter extends Person {

	private int spiele;
	
	public Schiedsrichter(int spiele) {
		super("", "", true);
		this.spiele=spiele;
	}

	public int getSpiele() {
		return spiele;
	}

	public void setSpiele(int spiele) {
		this.spiele = spiele;
	}
	
	
	
}
