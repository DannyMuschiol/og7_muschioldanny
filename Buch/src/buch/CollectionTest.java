package buch;

	import java.util.Scanner;
	import java.util.*;

	public class CollectionTest {
		
		static void menue() {
			System.out.println("\n ***** Buch-Verwaltung *******");
			System.out.println(" 1) eintragen ");
			System.out.println(" 2) finden ");
			System.out.println(" 3) l�schen");
			System.out.println(" 4) Die gr��te ISBN");
			System.out.println(" 5) zeigen");
			System.out.println(" 9) Beenden");
			System.out.println(" ********************");
			System.out.print(" Bitte die Auswahl treffen: ");
		} // menue

		public static void main(String[] args) {
			// TODO Auto-generated method stub

			List<Buch> buchliste = new LinkedList<Buch>();
	        Scanner scanny = new Scanner(System.in);

			char wahl;
			
			String autor;
			String isbn;
			String titel;
			do {
				menue();
				wahl = scanny.next().charAt(0);
				switch (wahl) {
				case '1':
					scanny.nextLine();
					System.out.print("Autor:" );
	                autor = scanny.nextLine();
	                System.out.print("Titel:" );
	                titel = scanny.nextLine();
	                System.out.print("ISBN:" );
	                isbn = scanny.nextLine();
	                buchliste.add(new Buch(autor, titel, isbn));
	                System.out.println(buchliste);
					break;
				case '2':
					scanny.nextLine();
					System.out.println("ISBN:");
					isbn = scanny.nextLine();
					System.out.println(findeBuch(buchliste, isbn));
					break;
				case '3':
					scanny.nextLine();
					System.out.println("ISBN Des Buches, welches gel�scht werden soll: ");
					isbn = scanny.nextLine();
					
					Buch buch;
					for (int i = 0; i < buchliste.size(); i++) {
						if (buchliste.get(i).getIsbn().equals(isbn)) {
							buch = buchliste.get(i);
							loescheBuch(buchliste, buch);
						}
					}
					
					break;
				case '4':
					System.out.println(ermitteleGroessteISBN(buchliste));
					break;		
				case '5':
					
					for (int i = 0; i < buchliste.size(); i++) {
						System.out.println(buchliste.get(i));
					}
					
					break;
				case '9':
					System.exit(0);
					break;
				default:
					menue();
					wahl = scanny.next().charAt(0);
			} // switch

		} while (wahl != 9);
			
			scanny.close();
	}// main

		public static Buch findeBuch(List<Buch> buchliste, String isbn) {
			
			for (int i = 0; i < buchliste.size(); i++) {
				if (isbn.equalsIgnoreCase(buchliste.get(i).getIsbn())) {
					return buchliste.get(i);
				}
			}
			
			return null;
		}

		public static boolean loescheBuch(List<Buch> buchliste, Buch buch) {
			
			return buchliste.remove(buch);
		}

		public static String ermitteleGroessteISBN(List<Buch> buchliste) {
			
			String groesste = "";
			
			for (int i = 0; i < buchliste.size(); i++) {
				if (buchliste.get(i).getIsbn().compareTo(groesste) > 0) {
					groesste = buchliste.get(i).getIsbn();
				}
			}
			if (!groesste.equals("")) {
				return groesste;
			}
			return null;
	 }

	}
