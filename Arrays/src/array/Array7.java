package array;

import java.util.Scanner;

public class Array7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan=new Scanner(System.in);
		
		int x;
		int y;
		
		System.out.print("Bitte den ersten Wert eingeben: ");
		x=scan.nextInt();
		
		System.out.print("Bitte den zweiten Wert eingeben: ");
		y=scan.nextInt();
		
		int[][] werte= new int[x][y];
		
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				werte[j][i]=i*j;
				if(j<x-1)
				System.out.print(werte[j][i] +", \t");
				else {
					System.out.print(werte[j][i]);	
				}
			}		System.out.println();
		}

		
	}

}
