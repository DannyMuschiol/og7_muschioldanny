package test;

import java.util.Scanner;

public class Prim {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);

		Stoppuhr uhr = new Stoppuhr();

		long x;
		int auswahl;

		System.out.print("M�chten Sie eine einzelne Zahl (1) oder mehrere Zahlen bis zu einer Grenze (2) berechnen: ");
		auswahl = scan.nextInt();

		switch (auswahl) {
		case 1:
			System.out.print("Zahl eingeben: ");
			x = scan.nextLong();
			uhr.start();
			Prim.isPrimE(x);
			uhr.stopp();
			System.out.print("Es dauerte " + uhr.dauer() + " Millisekunden");
			break;
		case 2:
			System.out.print("Zahl eingeben: ");
			x = scan.nextLong();
			uhr.start();
			Prim.isPrimM(x);
			uhr.stopp();
			System.out.print("Es dauerte " + uhr.dauer() + " Millisekunden");
			break;
		default:
			System.out.println("Fehler");
			break;
		}
		scan.close();
	}

	public static void isPrimM(long zahl) {
		boolean isPrim = true;
		zahl = Math.abs(zahl);

		if (zahl < 2) {
			isPrim = false;
		} else {
			for (long i = 2; i <= zahl; i++) {
				isPrim = true;

				for (long j = 2; j <= i - 1; j++) {
					if (i % j == 0)
						isPrim = false;
				}

				System.out.println(i + " ist " + isPrim);

			}
		}
	}

	public static void isPrimE(long zahl) {
		boolean isPrim = true;
		zahl = Math.abs(zahl);

		if (zahl < 2) {
			isPrim = false;
		} else {
			for (long i = 2; i < zahl; i++) {
				if (zahl % i == 0) {
					isPrim = false;
				}
			}
			System.out.println(zahl + " ist " + isPrim);
		}

	}
}
