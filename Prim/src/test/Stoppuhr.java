package test;

public class Stoppuhr {
	private long start;
	private long stopp;
	
	
	public Stoppuhr() {
		
	}
	
	public void start() {
		this.start=System.currentTimeMillis();
	}
	
	public void stopp() {
		this.stopp=System.currentTimeMillis();
	}
	
	public long dauer() {
		return stopp-start;
	}
}
