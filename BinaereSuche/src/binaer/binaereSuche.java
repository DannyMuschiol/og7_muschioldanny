package binaer;

public class binaereSuche {

	public static void main(String[] args) {
		
		int[] zahlenliste = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; 

		binaer(zahlenliste, 9, 0, zahlenliste.length-1, 0);
		
		
		
	}

	public static void binaer(int zahlenliste[], int gesuchteZahl, int links, int rechts, int mitte) {
    
	{
    	do
    	{
    		mitte = (rechts + links) / 2;

    		if(zahlenliste[mitte] < gesuchteZahl) {
    			links = mitte + 1;
    		}
            else {
            	rechts = mitte - 1;
            }
    	}
    	while(zahlenliste[mitte] != gesuchteZahl && links <= rechts);
       
        if(zahlenliste[mitte] == gesuchteZahl) {
            System.out.println("Position: " + (mitte+1) +" (" +mitte +")");
        }
        else {
            System.out.println("gesuchte Zahl nicht vorhanden!");
        }
    }
	
	}
	
}
