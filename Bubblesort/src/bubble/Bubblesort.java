package bubble;

import java.util.Random;

public class Bubblesort {

	public static void main(String[] args) {
		
		Random rand = new Random();
		
		int[] zahlen = new int[10];
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = rand.nextInt(100);
		}
		
		
		long start = System.currentTimeMillis();
		bubblesort(zahlen);
		long ende = System.currentTimeMillis();
		long zeit = ende-start;
		System.out.println("\n" +zeit);
		
		System.out.println();
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i] +", ");
		}
		
		
	}

	public static void bubblesort(int zahlenliste[]) {
		
		for (int i = 1; i < zahlenliste.length; i++) {
			
			for (int j = 0; j < zahlenliste.length-i ; j++) {
				
				int zahl = zahlenliste[j];
				
				if (zahlenliste[j] > zahlenliste[j+1]) {
					zahlenliste[j] = zahlenliste[j+1];
					zahlenliste[j+1] = zahl;
				}
				
			}
			
		}
		
	}
	
}
