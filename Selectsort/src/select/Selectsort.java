package select;

import java.util.Random;

public class Selectsort {

	public static void main(String[] args) {	
	
		Random rand = new Random();
		
		int[] zahlenUnsortiert = new int[10];
		
		for (int i = 0; i < zahlenUnsortiert.length; i++) {
			zahlenUnsortiert[i] = rand.nextInt(1000);
			if (i < zahlenUnsortiert.length-1) {
				System.out.print(zahlenUnsortiert[i] +", ");
			}
			else {
				System.out.print(zahlenUnsortiert[i]);
			}
		}
		
		System.out.println();
		long start = System.currentTimeMillis();
		int[] zahlenSortiert = selectsort(zahlenUnsortiert);
		long ende = System.currentTimeMillis();
		long zeit = ende-start;
		System.out.println("Zeit: " +zeit +" Millisekunden");
		
	}

	public static void arrayAusgabe(int zahlenliste[]) {
		for (int i = 0; i < zahlenliste.length; i++) {
			if (i < zahlenliste.length-1) {
				System.out.print(zahlenliste[i] +", ");
			}
			else {
				System.out.print(zahlenliste[i]);
			}
		}
	}
	
    public static int[] selectsort(int zahlenliste[]) {
        int q, speicher;
        for (int i = zahlenliste.length - 1; i >= 1; i--) {
            q = 0;
            for (int j = 1; j <= i; j++) {
                if (zahlenliste[j] > zahlenliste[q]) {
                    q = j;
                }
            }
            speicher = zahlenliste[q];
            zahlenliste[q] = zahlenliste[i];
            zahlenliste[i] = speicher;
            arrayAusgabe(zahlenliste);
            System.out.println();
        }
        return zahlenliste;
    }
	
}
