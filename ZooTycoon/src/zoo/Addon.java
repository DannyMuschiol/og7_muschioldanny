package zoo;

public class Addon {

	private double preis;
	private int id;
	private int maxBestand;
	private String bezeichnung;

	public Addon() {

	}

	public int bestandZaehler(int bestand) {

		return bestand;
	}

	public double wertRechner(int anzahl) {
		double wert = 0.0;
		return wert;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

}
