package fib;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);

		int monate;
		
		System.out.println("Anzahl der Monate: ");
		monate=scan.nextInt();
		
		System.out.println(fibunacci(monate));

	}
	
	public static int fibunacci(int monate) {
		if (monate == 1 || monate == 2) {        //Abbruchbedingung
			return 1;
		}	
		  else {
			  return fibunacci(monate-1) + fibunacci(monate-2); //Rekursionsaufruf
		  }
	}

}
