package omnom;

public class Haustier {

private int hunger;
private int muede;
private int zufrieden;
private int gesund;
private String name;

public Haustier() {
	hunger=100;
	muede=100;
	zufrieden=100;
	gesund=100;
}

public Haustier(String name ) {
	this.name=name;	
}

public int getHunger() {
	return hunger;
}

public void setHunger(int hunger) {
	this.hunger = hunger;
}

public int getMuede() {
	return muede;
}

public void setMuede(int muede) {
	this.muede = muede;
}

public int getZufrieden() {
	return zufrieden;
}

public void setZufrieden(int zufrieden) {
	this.zufrieden = zufrieden;
}

public int getGesund() {
	return gesund;
}

public void setGesund(int gesund) {
	this.gesund = gesund;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public void fuettern(int hunger) {
	int futter = this.getHunger()+hunger;
	
	if (futter>100) {
		futter=100;	
	}
	
	if (futter<0) {
		futter=0;	
	}
	this.setHunger(futter);
}

public void schlafen(int sZeit) {
	int schlafen = this.getMuede()+sZeit;
	
	if(schlafen>100) {
		schlafen=100;
	}
	
	if(schlafen<0) {
		schlafen=0;
	}
	
	this.setMuede(schlafen);
}

public void spielen(int f) {
	int zufrieden=this.getZufrieden()+f;
	
	if(zufrieden>100) {
		zufrieden=100;
	}
	
	if(zufrieden<0) {
		zufrieden=0;
	}
	
	this.setZufrieden(zufrieden);
}

public void heilen() {
	int heilung=100;
	
	
	this.setGesund(heilung);
}

}
